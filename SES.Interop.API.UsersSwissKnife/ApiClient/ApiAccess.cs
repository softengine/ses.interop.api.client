﻿using System;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using SES.Interop.API.PublicInterface;

namespace SES.Interop.API.UsersSwissKnife.ApiClient
{
    public static class ApiAccess
    {
        public static string GetServerTime(string base_url)
        {
            // Simple GET operation

            const string method = "/utctime";

            var api_uri = new Uri(base_url);
            var uri_builder = new UriBuilder(api_uri);
            uri_builder.Path += method;

            var request_url = uri_builder.Uri;
            var web_request = (HttpWebRequest)WebRequest.Create(request_url);
            web_request.Method = "GET";
            web_request.Accept = "application/json";

            try
            {
                var response = web_request.GetResponse();
                var response_stream = response.GetResponseStream();
                return response_stream == null
                    ? ""
                    : new StreamReader(response_stream).ReadToEnd();
            }
            catch (WebException wex)
            {
                var response_code = wex.Response == null ? "" : ((HttpWebResponse)wex.Response).StatusCode.ToString();
                var response_string = wex.Response == null ? "" : new StreamReader(wex.Response.GetResponseStream()).ReadToEnd();
                var message = string.Format("WebCall failed: {0} => {1}:({2}) {3}", request_url, wex.Message, response_code, response_string);
                throw new Exception(message, wex);
            }
        }

        public static string GetLocalTime(string base_url)
        {
            return DateTime.UtcNow.ToString("o");
        }

        public static ApiResult ExecuteCommand(string base_url, ApiCommand command, int timeout_sec)
        {
            const string method = "/execute";

            // Construct call URI
            var api_uri = new Uri(base_url);
            var uri_builder = new UriBuilder(api_uri);
            uri_builder.Path += method;

            // Setup WebRequest
            var request_url = uri_builder.Uri;
            var web_request = (HttpWebRequest)WebRequest.Create(request_url);
            web_request.Method = "POST";
            web_request.Accept = "application/json";

            // Pack command as content
            web_request.ContentType = "application/json";
            var jcontent = JsonConvert.SerializeObject(command);
            var data = new ASCIIEncoding().GetBytes(jcontent);
            web_request.ContentLength = data.Length;
            using (var content_stream = web_request.GetRequestStream())
                content_stream.Write(data, 0, data.Length);
            web_request.Timeout = timeout_sec*1000;

            // Execute
            var jrez = "";
            try
            {
                var response = web_request.GetResponse();
                var response_stream = response.GetResponseStream();
                jrez = response_stream == null
                    ? ""
                    : new StreamReader(response_stream).ReadToEnd();
            }
            catch (WebException wex)
            {
                var response_code = wex.Response == null ? "" : ((HttpWebResponse)wex.Response).StatusCode.ToString();
                var response_string = wex.Response == null ? "" : new StreamReader(wex.Response.GetResponseStream()).ReadToEnd();
                var message = string.Format("WebCall failed: {0} => {1}:({2}) {3}", request_url, wex.Message, response_code, response_string);
                throw new Exception(message, wex);
            }
            catch (Exception ex)
            {
                throw new Exception("WebCall failed: " + ex.Message, ex);
            }

            // try to deserialize response
            ApiResult rez;
            try
            {
                rez = JsonConvert.DeserializeObject<ApiResult>(jrez);
            }
            catch (Exception e)
            {
                throw new Exception("Webcall unexpected response: \r\n" + jrez, e);
            }

            return rez;
        }
    }
}
