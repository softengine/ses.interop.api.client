﻿namespace SES.Interop.API.UsersSwissKnife.ApiClient.Model
{
    public class SimpleKeyPair
    {
        public string PublicKey { get; set; }
        public string PrivateKey { get; set; }
    }
}
