﻿using System.Security.Cryptography;
using SES.Interop.API.PublicInterface;
using SES.Interop.API.UsersSwissKnife.ApiClient.Model;

namespace SES.Interop.API.UsersSwissKnife.ApiClient
{
    public static class KeysManagement
    {
        public static SimpleKeyPair CreateKeyPair()
        {
            using (var rsa = new RSACryptoServiceProvider(ApiParameters.KeyLength))
                try
                {
                    return new SimpleKeyPair
                    {
                        PrivateKey = rsa.ToXmlString(true),
                        PublicKey = rsa.ToXmlString(false),
                    };
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
        }

        public static void SaveToFile(SimpleKeyPair key, string path)
        {
            System.IO.File.WriteAllText(path, key.PrivateKey);
        }

        public static SimpleKeyPair LoadFromFile(string path)
        {
            using (var rsa = new RSACryptoServiceProvider())
                try
                {
                    var private_key = System.IO.File.ReadAllText(path);

                    rsa.FromXmlString(private_key);

                    return new SimpleKeyPair
                    {
                        PrivateKey = rsa.ToXmlString(true),
                        PublicKey = rsa.ToXmlString(false),
                    };
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
        }
    }
}
