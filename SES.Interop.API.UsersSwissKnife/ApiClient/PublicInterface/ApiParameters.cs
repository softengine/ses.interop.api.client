﻿namespace SES.Interop.API.PublicInterface
{
    public static class ApiParameters
    {
        public const int KeyLength = 2048;
        public const string SigningAlgorithm = "SHA512";

        public static string SigningBase(ApiCommand command)
        {
            return string.Format("{0}@@{1}@@{2}@@{3}",
                command.UserName,
                command.UtcTimestamp.ToString("yyyyMMddhhmmssffff"),
                command.Command,
                command.Parameters
                );
        }
    }
}
