﻿namespace SES.Interop.API.PublicInterface
{
    public class ApiResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public string Content { get; set; }

        public static ApiResult Failed(string token)
        {
            return new ApiResult
            {
                Success = false,
                Message = string.Format("API call failed. Please contact support. Error Record Id:{0};", token),
                Content = "",
            };
        }
    }
}
