﻿using System.Collections.Generic;

namespace SES.Interop.API.PublicInterface.Command
{
    public class ReadOperationParameters
    {
        public Dictionary<string,string> Parameters { get; set; }
    }
}
