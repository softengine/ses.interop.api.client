﻿namespace SES.Interop.API.PublicInterface.Command
{
    public class WriteOperationParameters
    {
        public string Data { get; set; }
    }
}