﻿using System;

namespace SES.Interop.API.PublicInterface
{
    public class ApiCommand
    {
        public string UserName { get; set; }
        public DateTime UtcTimestamp { get; set; }
        public string Command { get; set; }
        public string Parameters { get; set; }
        public string Signature { get; set; }
    }
}
