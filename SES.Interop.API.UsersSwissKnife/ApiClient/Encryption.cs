﻿using System;
using System.Security.Cryptography;
using System.Text;
using SES.Interop.API.PublicInterface;

namespace SES.Interop.API.UsersSwissKnife.ApiClient
{
    public static class Encryption
    {
        public static string SignMessage(ApiCommand message, string private_key)
        {
            var signing_text = ApiParameters.SigningBase(message);

            using (var rsa = new RSACryptoServiceProvider())
                try
                {
                    rsa.FromXmlString(private_key);

                    var encoder = new UTF8Encoding();
                    var signing_data = encoder.GetBytes(signing_text);

                    var signature_data = rsa.SignData(signing_data, CryptoConfig.MapNameToOID(ApiParameters.SigningAlgorithm));

                    var rez = Convert.ToBase64String(signature_data);
                    return rez;
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
        }
    }
}
