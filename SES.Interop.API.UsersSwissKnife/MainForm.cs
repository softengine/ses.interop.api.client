﻿using System;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Newtonsoft.Json;
using SES.Interop.API.PublicInterface;
using SES.Interop.API.UsersSwissKnife.ApiClient;
using SES.Interop.API.UsersSwissKnife.ApiClient.Model;
using SES.Interop.API.UsersSwissKnife.Properties;
using Formatting = Newtonsoft.Json.Formatting;

namespace SES.Interop.API.UsersSwissKnife
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        #region Header

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Settings.Default.Save();
        }

        #endregion

        #region Keys tab

        private void bKeyNew_Click(object sender, System.EventArgs e)
        {
            var new_key = KeysManagement.CreateKeyPair();
            tbPrivKey.Text = new_key.PrivateKey;
            tbPubKey.Text = new_key.PublicKey;
        }

        private void bKeySave_Click(object sender, System.EventArgs e)
        {
            if (sfdKeySave.ShowDialog() != DialogResult.OK) return;

            try
            {
                var key = new SimpleKeyPair
                {
                    PublicKey = tbPubKey.Text,
                    PrivateKey = tbPrivKey.Text,
                };

                KeysManagement.SaveToFile(key, sfdKeySave.FileName);
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message, "Can't save key", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bKeyOpen_Click(object sender, System.EventArgs e)
        {
            if (ofdKeyOpen.ShowDialog() != DialogResult.OK) return;

            try
            {
                var key = KeysManagement.LoadFromFile(ofdKeyOpen.FileName);
                tbPrivKey.Text = key.PrivateKey;
                tbPubKey.Text = key.PublicKey;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message, "Can't open key", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Request

        private void bReqServerTimeRefresh_Click(object sender, System.EventArgs e)
        {
            try
            {
                tbReqServerTime.Text = ApiAccess.GetServerTime(tbEntryPointUrl.Text);
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message, "Can't request server time", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bReqLocalTimeRefresh_Click(object sender, System.EventArgs e)
        {
            try
            {
                tbReqLocalTime.Text = ApiAccess.GetLocalTime(tbEntryPointUrl.Text);
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message, "Can't request local time", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bReqSignMessage_Click(object sender, System.EventArgs e)
        {
            try
            {
                var message = CollectMessage();

                if (cbReqUseActualTime.Checked)
                {
                    bReqLocalTimeRefresh_Click(sender, e);
                    message.UtcTimestamp = DateTime.UtcNow;
                }

                var private_key = tbPrivKey.Text;
                if(string.IsNullOrEmpty(private_key)) throw new Exception("Key is not loaded");
                var signature = Encryption.SignMessage(message, private_key);
                tbReqSignature.Text = signature;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message, "Can't sign message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bReqSend_Click(object sender, EventArgs e)
        {
            try
            {
                var message = CollectMessage();

                if (cbReqUseActualTime.Checked)
                {
                    bReqLocalTimeRefresh_Click(sender, e);
                    message.UtcTimestamp = DateTime.UtcNow;
                }

                if (cbReqUseActualSignature.Checked)
                {
                    var private_key = tbPrivKey.Text;
                    if (string.IsNullOrEmpty(private_key)) throw new Exception("Key is not loaded");
                    var signature = Encryption.SignMessage(message, private_key);
                    tbReqSignature.Text = signature;
                    message.Signature = signature;
                }

                var response = ApiAccess.ExecuteCommand(tbEntryPointUrl.Text, message, (int)nudReqTimeout.Value);

                cbResSuccess.CheckState = response.Success ? CheckState.Checked : CheckState.Unchecked;
                tbResMessage.Text = response.Message;
                tbResponse.Text = response.Content;

                tcTabs.SelectTab(tpResponse);
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message, "Can't send message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private ApiCommand CollectMessage()
        {
            return new ApiCommand
            {
                UserName = tbUserName.Text,
                UtcTimestamp = string.IsNullOrEmpty(tbReqLocalTime.Text) ? DateTime.UtcNow : DateTime.Parse(tbReqLocalTime.Text),
                Command = cbReqCommand.Text,
                Parameters = tbReqParams.Text,
                Signature = tbReqSignature.Text,
            };
        }

        #endregion

        #region Response

        private void bRespJson_Click(object sender, EventArgs e)
        {
            try
            {
                var json = tbResponse.Text;
                var jobject = JsonConvert.DeserializeObject(json);
                var json_formatted = JsonConvert.SerializeObject(jobject, Formatting.Indented);
                tbResponse.Text = json_formatted;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message, "Can't format output to JSON", MessageBoxButtons.OK, MessageBoxIcon.Error);                
            }
        }

        private void bRespXml_Click(object sender, EventArgs e)
        {
            try
            {
                var xml = tbResponse.Text;
                var xdoc = new XmlDocument();
                xdoc.LoadXml(xml);

                var sbuilder = new StringBuilder();
                var xmlwriter_settings = new XmlWriterSettings { Indent = true, };
                using (var xmlwriter = XmlWriter.Create(sbuilder, xmlwriter_settings))
                    xdoc.Save(xmlwriter);

                tbResponse.Text = sbuilder.ToString();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message, "Can't format output to XML", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bRespSave_Click(object sender, EventArgs e)
        {
            if (sfdResSave.ShowDialog() != DialogResult.OK) return;

            try
            {
                System.IO.File.WriteAllText(sfdResSave.FileName, tbResponse.Text);
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message, "Can't save output", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion
    }
}
