﻿namespace SES.Interop.API.UsersSwissKnife
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lEntryPoint = new System.Windows.Forms.Label();
            this.lUserName = new System.Windows.Forms.Label();
            this.tcTabs = new System.Windows.Forms.TabControl();
            this.tpKey = new System.Windows.Forms.TabPage();
            this.scKeys = new System.Windows.Forms.SplitContainer();
            this.gbPrivKey = new System.Windows.Forms.GroupBox();
            this.tbPrivKey = new System.Windows.Forms.TextBox();
            this.gbPubKey = new System.Windows.Forms.GroupBox();
            this.tbPubKey = new System.Windows.Forms.TextBox();
            this.bKeyRequestRenew = new System.Windows.Forms.Button();
            this.bKeyOpen = new System.Windows.Forms.Button();
            this.bKeySave = new System.Windows.Forms.Button();
            this.bKeyNew = new System.Windows.Forms.Button();
            this.tpRequest = new System.Windows.Forms.TabPage();
            this.nudReqTimeout = new System.Windows.Forms.NumericUpDown();
            this.lReqTimeout = new System.Windows.Forms.Label();
            this.bReqSignMessage = new System.Windows.Forms.Button();
            this.cbReqUseActualSignature = new System.Windows.Forms.CheckBox();
            this.cbReqUseActualTime = new System.Windows.Forms.CheckBox();
            this.bReqCommandsRefresh = new System.Windows.Forms.Button();
            this.cbReqCommand = new System.Windows.Forms.ComboBox();
            this.lReqCommand = new System.Windows.Forms.Label();
            this.tbReqSignature = new System.Windows.Forms.TextBox();
            this.tbReqParams = new System.Windows.Forms.TextBox();
            this.lReqSignature = new System.Windows.Forms.Label();
            this.lReqParams = new System.Windows.Forms.Label();
            this.bReqSend = new System.Windows.Forms.Button();
            this.bReqLocalTimeRefresh = new System.Windows.Forms.Button();
            this.bReqServerTimeRefresh = new System.Windows.Forms.Button();
            this.tbReqLocalTime = new System.Windows.Forms.TextBox();
            this.tbReqServerTime = new System.Windows.Forms.TextBox();
            this.lReqLocalTime = new System.Windows.Forms.Label();
            this.lReqServerTime = new System.Windows.Forms.Label();
            this.tpResponse = new System.Windows.Forms.TabPage();
            this.tbResMessage = new System.Windows.Forms.TextBox();
            this.lResMessage = new System.Windows.Forms.Label();
            this.cbResSuccess = new System.Windows.Forms.CheckBox();
            this.bRespSave = new System.Windows.Forms.Button();
            this.tbResponse = new System.Windows.Forms.TextBox();
            this.bRespXml = new System.Windows.Forms.Button();
            this.bRespJson = new System.Windows.Forms.Button();
            this.sfdKeySave = new System.Windows.Forms.SaveFileDialog();
            this.ofdKeyOpen = new System.Windows.Forms.OpenFileDialog();
            this.tbUserName = new System.Windows.Forms.TextBox();
            this.tbEntryPointUrl = new System.Windows.Forms.TextBox();
            this.sfdResSave = new System.Windows.Forms.SaveFileDialog();
            this.tcTabs.SuspendLayout();
            this.tpKey.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scKeys)).BeginInit();
            this.scKeys.Panel1.SuspendLayout();
            this.scKeys.Panel2.SuspendLayout();
            this.scKeys.SuspendLayout();
            this.gbPrivKey.SuspendLayout();
            this.gbPubKey.SuspendLayout();
            this.tpRequest.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudReqTimeout)).BeginInit();
            this.tpResponse.SuspendLayout();
            this.SuspendLayout();
            // 
            // lEntryPoint
            // 
            this.lEntryPoint.AutoSize = true;
            this.lEntryPoint.Location = new System.Drawing.Point(12, 15);
            this.lEntryPoint.Name = "lEntryPoint";
            this.lEntryPoint.Size = new System.Drawing.Size(58, 13);
            this.lEntryPoint.TabIndex = 0;
            this.lEntryPoint.Text = "Entry Point";
            // 
            // lUserName
            // 
            this.lUserName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lUserName.AutoSize = true;
            this.lUserName.Location = new System.Drawing.Point(447, 15);
            this.lUserName.Name = "lUserName";
            this.lUserName.Size = new System.Drawing.Size(29, 13);
            this.lUserName.TabIndex = 2;
            this.lUserName.Text = "User";
            // 
            // tcTabs
            // 
            this.tcTabs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcTabs.Controls.Add(this.tpKey);
            this.tcTabs.Controls.Add(this.tpRequest);
            this.tcTabs.Controls.Add(this.tpResponse);
            this.tcTabs.Location = new System.Drawing.Point(12, 38);
            this.tcTabs.Name = "tcTabs";
            this.tcTabs.SelectedIndex = 0;
            this.tcTabs.Size = new System.Drawing.Size(584, 390);
            this.tcTabs.TabIndex = 4;
            // 
            // tpKey
            // 
            this.tpKey.Controls.Add(this.scKeys);
            this.tpKey.Controls.Add(this.bKeyRequestRenew);
            this.tpKey.Controls.Add(this.bKeyOpen);
            this.tpKey.Controls.Add(this.bKeySave);
            this.tpKey.Controls.Add(this.bKeyNew);
            this.tpKey.Location = new System.Drawing.Point(4, 22);
            this.tpKey.Name = "tpKey";
            this.tpKey.Padding = new System.Windows.Forms.Padding(3);
            this.tpKey.Size = new System.Drawing.Size(576, 364);
            this.tpKey.TabIndex = 0;
            this.tpKey.Text = "Key";
            this.tpKey.UseVisualStyleBackColor = true;
            // 
            // scKeys
            // 
            this.scKeys.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scKeys.Location = new System.Drawing.Point(6, 6);
            this.scKeys.Name = "scKeys";
            this.scKeys.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scKeys.Panel1
            // 
            this.scKeys.Panel1.Controls.Add(this.gbPrivKey);
            // 
            // scKeys.Panel2
            // 
            this.scKeys.Panel2.Controls.Add(this.gbPubKey);
            this.scKeys.Size = new System.Drawing.Size(474, 355);
            this.scKeys.SplitterDistance = 177;
            this.scKeys.TabIndex = 7;
            // 
            // gbPrivKey
            // 
            this.gbPrivKey.Controls.Add(this.tbPrivKey);
            this.gbPrivKey.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbPrivKey.Location = new System.Drawing.Point(0, 0);
            this.gbPrivKey.Name = "gbPrivKey";
            this.gbPrivKey.Size = new System.Drawing.Size(474, 177);
            this.gbPrivKey.TabIndex = 0;
            this.gbPrivKey.TabStop = false;
            this.gbPrivKey.Text = "Private Key";
            // 
            // tbPrivKey
            // 
            this.tbPrivKey.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbPrivKey.Location = new System.Drawing.Point(3, 16);
            this.tbPrivKey.Multiline = true;
            this.tbPrivKey.Name = "tbPrivKey";
            this.tbPrivKey.ReadOnly = true;
            this.tbPrivKey.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbPrivKey.Size = new System.Drawing.Size(468, 158);
            this.tbPrivKey.TabIndex = 0;
            // 
            // gbPubKey
            // 
            this.gbPubKey.Controls.Add(this.tbPubKey);
            this.gbPubKey.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbPubKey.Location = new System.Drawing.Point(0, 0);
            this.gbPubKey.Name = "gbPubKey";
            this.gbPubKey.Size = new System.Drawing.Size(474, 174);
            this.gbPubKey.TabIndex = 1;
            this.gbPubKey.TabStop = false;
            this.gbPubKey.Text = "Public Key";
            // 
            // tbPubKey
            // 
            this.tbPubKey.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbPubKey.Location = new System.Drawing.Point(3, 16);
            this.tbPubKey.Multiline = true;
            this.tbPubKey.Name = "tbPubKey";
            this.tbPubKey.ReadOnly = true;
            this.tbPubKey.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbPubKey.Size = new System.Drawing.Size(468, 155);
            this.tbPubKey.TabIndex = 0;
            // 
            // bKeyRequestRenew
            // 
            this.bKeyRequestRenew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bKeyRequestRenew.Enabled = false;
            this.bKeyRequestRenew.Location = new System.Drawing.Point(486, 312);
            this.bKeyRequestRenew.Name = "bKeyRequestRenew";
            this.bKeyRequestRenew.Size = new System.Drawing.Size(84, 46);
            this.bKeyRequestRenew.TabIndex = 6;
            this.bKeyRequestRenew.Text = "Request Renew";
            this.bKeyRequestRenew.UseVisualStyleBackColor = true;
            // 
            // bKeyOpen
            // 
            this.bKeyOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bKeyOpen.Location = new System.Drawing.Point(486, 80);
            this.bKeyOpen.Name = "bKeyOpen";
            this.bKeyOpen.Size = new System.Drawing.Size(84, 23);
            this.bKeyOpen.TabIndex = 4;
            this.bKeyOpen.Text = "Open";
            this.bKeyOpen.UseVisualStyleBackColor = true;
            this.bKeyOpen.Click += new System.EventHandler(this.bKeyOpen_Click);
            // 
            // bKeySave
            // 
            this.bKeySave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bKeySave.Location = new System.Drawing.Point(486, 51);
            this.bKeySave.Name = "bKeySave";
            this.bKeySave.Size = new System.Drawing.Size(84, 23);
            this.bKeySave.TabIndex = 3;
            this.bKeySave.Text = "Save";
            this.bKeySave.UseVisualStyleBackColor = true;
            this.bKeySave.Click += new System.EventHandler(this.bKeySave_Click);
            // 
            // bKeyNew
            // 
            this.bKeyNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bKeyNew.Location = new System.Drawing.Point(486, 22);
            this.bKeyNew.Name = "bKeyNew";
            this.bKeyNew.Size = new System.Drawing.Size(84, 23);
            this.bKeyNew.TabIndex = 2;
            this.bKeyNew.Text = "New";
            this.bKeyNew.UseVisualStyleBackColor = true;
            this.bKeyNew.Click += new System.EventHandler(this.bKeyNew_Click);
            // 
            // tpRequest
            // 
            this.tpRequest.Controls.Add(this.nudReqTimeout);
            this.tpRequest.Controls.Add(this.lReqTimeout);
            this.tpRequest.Controls.Add(this.bReqSignMessage);
            this.tpRequest.Controls.Add(this.cbReqUseActualSignature);
            this.tpRequest.Controls.Add(this.cbReqUseActualTime);
            this.tpRequest.Controls.Add(this.bReqCommandsRefresh);
            this.tpRequest.Controls.Add(this.cbReqCommand);
            this.tpRequest.Controls.Add(this.lReqCommand);
            this.tpRequest.Controls.Add(this.tbReqSignature);
            this.tpRequest.Controls.Add(this.tbReqParams);
            this.tpRequest.Controls.Add(this.lReqSignature);
            this.tpRequest.Controls.Add(this.lReqParams);
            this.tpRequest.Controls.Add(this.bReqSend);
            this.tpRequest.Controls.Add(this.bReqLocalTimeRefresh);
            this.tpRequest.Controls.Add(this.bReqServerTimeRefresh);
            this.tpRequest.Controls.Add(this.tbReqLocalTime);
            this.tpRequest.Controls.Add(this.tbReqServerTime);
            this.tpRequest.Controls.Add(this.lReqLocalTime);
            this.tpRequest.Controls.Add(this.lReqServerTime);
            this.tpRequest.Location = new System.Drawing.Point(4, 22);
            this.tpRequest.Name = "tpRequest";
            this.tpRequest.Padding = new System.Windows.Forms.Padding(3);
            this.tpRequest.Size = new System.Drawing.Size(576, 364);
            this.tpRequest.TabIndex = 1;
            this.tpRequest.Text = "Request";
            this.tpRequest.UseVisualStyleBackColor = true;
            // 
            // nudReqTimeout
            // 
            this.nudReqTimeout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.nudReqTimeout.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::SES.Interop.API.UsersSwissKnife.Properties.Settings.Default, "RequestTimeout", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.nudReqTimeout.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudReqTimeout.Location = new System.Drawing.Point(492, 338);
            this.nudReqTimeout.Maximum = new decimal(new int[] {
            3600,
            0,
            0,
            0});
            this.nudReqTimeout.Name = "nudReqTimeout";
            this.nudReqTimeout.Size = new System.Drawing.Size(78, 20);
            this.nudReqTimeout.TabIndex = 17;
            this.nudReqTimeout.Value = global::SES.Interop.API.UsersSwissKnife.Properties.Settings.Default.RequestTimeout;
            // 
            // lReqTimeout
            // 
            this.lReqTimeout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lReqTimeout.AutoSize = true;
            this.lReqTimeout.Location = new System.Drawing.Point(418, 340);
            this.lReqTimeout.Name = "lReqTimeout";
            this.lReqTimeout.Size = new System.Drawing.Size(68, 13);
            this.lReqTimeout.TabIndex = 16;
            this.lReqTimeout.Text = "Timeout, sec";
            // 
            // bReqSignMessage
            // 
            this.bReqSignMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bReqSignMessage.Location = new System.Drawing.Point(75, 307);
            this.bReqSignMessage.Name = "bReqSignMessage";
            this.bReqSignMessage.Size = new System.Drawing.Size(22, 22);
            this.bReqSignMessage.TabIndex = 15;
            this.bReqSignMessage.Text = "↺";
            this.bReqSignMessage.UseVisualStyleBackColor = true;
            this.bReqSignMessage.Click += new System.EventHandler(this.bReqSignMessage_Click);
            // 
            // cbReqUseActualSignature
            // 
            this.cbReqUseActualSignature.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbReqUseActualSignature.AutoSize = true;
            this.cbReqUseActualSignature.Checked = true;
            this.cbReqUseActualSignature.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbReqUseActualSignature.Location = new System.Drawing.Point(105, 311);
            this.cbReqUseActualSignature.Name = "cbReqUseActualSignature";
            this.cbReqUseActualSignature.Size = new System.Drawing.Size(176, 17);
            this.cbReqUseActualSignature.TabIndex = 14;
            this.cbReqUseActualSignature.Text = "Use actual signature on request";
            this.cbReqUseActualSignature.UseVisualStyleBackColor = true;
            // 
            // cbReqUseActualTime
            // 
            this.cbReqUseActualTime.AutoSize = true;
            this.cbReqUseActualTime.Checked = true;
            this.cbReqUseActualTime.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbReqUseActualTime.Location = new System.Drawing.Point(278, 34);
            this.cbReqUseActualTime.Name = "cbReqUseActualTime";
            this.cbReqUseActualTime.Size = new System.Drawing.Size(152, 17);
            this.cbReqUseActualTime.TabIndex = 14;
            this.cbReqUseActualTime.Text = "Use actual time on request";
            this.cbReqUseActualTime.UseVisualStyleBackColor = true;
            // 
            // bReqCommandsRefresh
            // 
            this.bReqCommandsRefresh.Enabled = false;
            this.bReqCommandsRefresh.Location = new System.Drawing.Point(249, 58);
            this.bReqCommandsRefresh.Name = "bReqCommandsRefresh";
            this.bReqCommandsRefresh.Size = new System.Drawing.Size(22, 23);
            this.bReqCommandsRefresh.TabIndex = 13;
            this.bReqCommandsRefresh.Text = "↺";
            this.bReqCommandsRefresh.UseVisualStyleBackColor = true;
            // 
            // cbReqCommand
            // 
            this.cbReqCommand.FormattingEnabled = true;
            this.cbReqCommand.Location = new System.Drawing.Point(76, 59);
            this.cbReqCommand.Name = "cbReqCommand";
            this.cbReqCommand.Size = new System.Drawing.Size(173, 21);
            this.cbReqCommand.TabIndex = 12;
            // 
            // lReqCommand
            // 
            this.lReqCommand.AutoSize = true;
            this.lReqCommand.Location = new System.Drawing.Point(6, 63);
            this.lReqCommand.Name = "lReqCommand";
            this.lReqCommand.Size = new System.Drawing.Size(54, 13);
            this.lReqCommand.TabIndex = 11;
            this.lReqCommand.Text = "Command";
            // 
            // tbReqSignature
            // 
            this.tbReqSignature.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbReqSignature.Location = new System.Drawing.Point(76, 255);
            this.tbReqSignature.Multiline = true;
            this.tbReqSignature.Name = "tbReqSignature";
            this.tbReqSignature.ReadOnly = true;
            this.tbReqSignature.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbReqSignature.Size = new System.Drawing.Size(494, 52);
            this.tbReqSignature.TabIndex = 10;
            // 
            // tbReqParams
            // 
            this.tbReqParams.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbReqParams.Location = new System.Drawing.Point(76, 87);
            this.tbReqParams.Multiline = true;
            this.tbReqParams.Name = "tbReqParams";
            this.tbReqParams.Size = new System.Drawing.Size(494, 162);
            this.tbReqParams.TabIndex = 9;
            // 
            // lReqSignature
            // 
            this.lReqSignature.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lReqSignature.AutoSize = true;
            this.lReqSignature.Location = new System.Drawing.Point(6, 258);
            this.lReqSignature.Name = "lReqSignature";
            this.lReqSignature.Size = new System.Drawing.Size(52, 13);
            this.lReqSignature.TabIndex = 8;
            this.lReqSignature.Text = "Signature";
            // 
            // lReqParams
            // 
            this.lReqParams.AutoSize = true;
            this.lReqParams.Location = new System.Drawing.Point(6, 87);
            this.lReqParams.Name = "lReqParams";
            this.lReqParams.Size = new System.Drawing.Size(60, 13);
            this.lReqParams.TabIndex = 7;
            this.lReqParams.Text = "Parameters";
            // 
            // bReqSend
            // 
            this.bReqSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bReqSend.Location = new System.Drawing.Point(76, 334);
            this.bReqSend.Name = "bReqSend";
            this.bReqSend.Size = new System.Drawing.Size(75, 23);
            this.bReqSend.TabIndex = 6;
            this.bReqSend.Text = "Send";
            this.bReqSend.UseVisualStyleBackColor = true;
            this.bReqSend.Click += new System.EventHandler(this.bReqSend_Click);
            // 
            // bReqLocalTimeRefresh
            // 
            this.bReqLocalTimeRefresh.Location = new System.Drawing.Point(249, 31);
            this.bReqLocalTimeRefresh.Name = "bReqLocalTimeRefresh";
            this.bReqLocalTimeRefresh.Size = new System.Drawing.Size(22, 22);
            this.bReqLocalTimeRefresh.TabIndex = 5;
            this.bReqLocalTimeRefresh.Text = "↺";
            this.bReqLocalTimeRefresh.UseVisualStyleBackColor = true;
            this.bReqLocalTimeRefresh.Click += new System.EventHandler(this.bReqLocalTimeRefresh_Click);
            // 
            // bReqServerTimeRefresh
            // 
            this.bReqServerTimeRefresh.Location = new System.Drawing.Point(249, 5);
            this.bReqServerTimeRefresh.Name = "bReqServerTimeRefresh";
            this.bReqServerTimeRefresh.Size = new System.Drawing.Size(22, 22);
            this.bReqServerTimeRefresh.TabIndex = 4;
            this.bReqServerTimeRefresh.Text = "↺";
            this.bReqServerTimeRefresh.UseVisualStyleBackColor = true;
            this.bReqServerTimeRefresh.Click += new System.EventHandler(this.bReqServerTimeRefresh_Click);
            // 
            // tbReqLocalTime
            // 
            this.tbReqLocalTime.Location = new System.Drawing.Point(76, 32);
            this.tbReqLocalTime.Name = "tbReqLocalTime";
            this.tbReqLocalTime.ReadOnly = true;
            this.tbReqLocalTime.Size = new System.Drawing.Size(173, 20);
            this.tbReqLocalTime.TabIndex = 3;
            // 
            // tbReqServerTime
            // 
            this.tbReqServerTime.Location = new System.Drawing.Point(76, 6);
            this.tbReqServerTime.Name = "tbReqServerTime";
            this.tbReqServerTime.ReadOnly = true;
            this.tbReqServerTime.Size = new System.Drawing.Size(173, 20);
            this.tbReqServerTime.TabIndex = 2;
            // 
            // lReqLocalTime
            // 
            this.lReqLocalTime.AutoSize = true;
            this.lReqLocalTime.Location = new System.Drawing.Point(6, 36);
            this.lReqLocalTime.Name = "lReqLocalTime";
            this.lReqLocalTime.Size = new System.Drawing.Size(59, 13);
            this.lReqLocalTime.TabIndex = 1;
            this.lReqLocalTime.Text = "Local Time";
            // 
            // lReqServerTime
            // 
            this.lReqServerTime.AutoSize = true;
            this.lReqServerTime.Location = new System.Drawing.Point(6, 10);
            this.lReqServerTime.Name = "lReqServerTime";
            this.lReqServerTime.Size = new System.Drawing.Size(64, 13);
            this.lReqServerTime.TabIndex = 0;
            this.lReqServerTime.Text = "Server Time";
            // 
            // tpResponse
            // 
            this.tpResponse.Controls.Add(this.tbResMessage);
            this.tpResponse.Controls.Add(this.lResMessage);
            this.tpResponse.Controls.Add(this.cbResSuccess);
            this.tpResponse.Controls.Add(this.bRespSave);
            this.tpResponse.Controls.Add(this.tbResponse);
            this.tpResponse.Controls.Add(this.bRespXml);
            this.tpResponse.Controls.Add(this.bRespJson);
            this.tpResponse.Location = new System.Drawing.Point(4, 22);
            this.tpResponse.Name = "tpResponse";
            this.tpResponse.Padding = new System.Windows.Forms.Padding(3);
            this.tpResponse.Size = new System.Drawing.Size(576, 364);
            this.tpResponse.TabIndex = 2;
            this.tpResponse.Text = "Response";
            this.tpResponse.UseVisualStyleBackColor = true;
            // 
            // tbResMessage
            // 
            this.tbResMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbResMessage.Location = new System.Drawing.Point(60, 6);
            this.tbResMessage.Name = "tbResMessage";
            this.tbResMessage.ReadOnly = true;
            this.tbResMessage.Size = new System.Drawing.Size(347, 20);
            this.tbResMessage.TabIndex = 9;
            // 
            // lResMessage
            // 
            this.lResMessage.AutoSize = true;
            this.lResMessage.Location = new System.Drawing.Point(6, 9);
            this.lResMessage.Name = "lResMessage";
            this.lResMessage.Size = new System.Drawing.Size(50, 13);
            this.lResMessage.TabIndex = 8;
            this.lResMessage.Text = "Message";
            // 
            // cbResSuccess
            // 
            this.cbResSuccess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbResSuccess.AutoSize = true;
            this.cbResSuccess.Checked = true;
            this.cbResSuccess.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.cbResSuccess.Enabled = false;
            this.cbResSuccess.Location = new System.Drawing.Point(413, 8);
            this.cbResSuccess.Name = "cbResSuccess";
            this.cbResSuccess.Size = new System.Drawing.Size(67, 17);
            this.cbResSuccess.TabIndex = 7;
            this.cbResSuccess.Text = "Success";
            this.cbResSuccess.UseVisualStyleBackColor = true;
            // 
            // bRespSave
            // 
            this.bRespSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bRespSave.Location = new System.Drawing.Point(486, 335);
            this.bRespSave.Name = "bRespSave";
            this.bRespSave.Size = new System.Drawing.Size(84, 23);
            this.bRespSave.TabIndex = 6;
            this.bRespSave.Text = "Save";
            this.bRespSave.UseVisualStyleBackColor = true;
            this.bRespSave.Click += new System.EventHandler(this.bRespSave_Click);
            // 
            // tbResponse
            // 
            this.tbResponse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbResponse.Location = new System.Drawing.Point(6, 32);
            this.tbResponse.Multiline = true;
            this.tbResponse.Name = "tbResponse";
            this.tbResponse.ReadOnly = true;
            this.tbResponse.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbResponse.Size = new System.Drawing.Size(474, 326);
            this.tbResponse.TabIndex = 5;
            // 
            // bRespXml
            // 
            this.bRespXml.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bRespXml.Location = new System.Drawing.Point(486, 61);
            this.bRespXml.Name = "bRespXml";
            this.bRespXml.Size = new System.Drawing.Size(84, 23);
            this.bRespXml.TabIndex = 4;
            this.bRespXml.Text = "XML";
            this.bRespXml.UseVisualStyleBackColor = true;
            this.bRespXml.Click += new System.EventHandler(this.bRespXml_Click);
            // 
            // bRespJson
            // 
            this.bRespJson.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bRespJson.Location = new System.Drawing.Point(486, 32);
            this.bRespJson.Name = "bRespJson";
            this.bRespJson.Size = new System.Drawing.Size(84, 23);
            this.bRespJson.TabIndex = 3;
            this.bRespJson.Text = "JSON";
            this.bRespJson.UseVisualStyleBackColor = true;
            this.bRespJson.Click += new System.EventHandler(this.bRespJson_Click);
            // 
            // sfdKeySave
            // 
            this.sfdKeySave.DefaultExt = "txt";
            this.sfdKeySave.Filter = "*.txt|*.txt";
            this.sfdKeySave.RestoreDirectory = true;
            // 
            // ofdKeyOpen
            // 
            this.ofdKeyOpen.DefaultExt = "txt";
            this.ofdKeyOpen.Filter = "*.txt|*.txt";
            this.ofdKeyOpen.RestoreDirectory = true;
            // 
            // tbUserName
            // 
            this.tbUserName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbUserName.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SES.Interop.API.UsersSwissKnife.Properties.Settings.Default, "ApiUser", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tbUserName.Location = new System.Drawing.Point(482, 12);
            this.tbUserName.Name = "tbUserName";
            this.tbUserName.Size = new System.Drawing.Size(114, 20);
            this.tbUserName.TabIndex = 3;
            this.tbUserName.Text = global::SES.Interop.API.UsersSwissKnife.Properties.Settings.Default.ApiUser;
            // 
            // tbEntryPointUrl
            // 
            this.tbEntryPointUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbEntryPointUrl.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SES.Interop.API.UsersSwissKnife.Properties.Settings.Default, "ApiUrl", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tbEntryPointUrl.Location = new System.Drawing.Point(76, 12);
            this.tbEntryPointUrl.Name = "tbEntryPointUrl";
            this.tbEntryPointUrl.Size = new System.Drawing.Size(365, 20);
            this.tbEntryPointUrl.TabIndex = 1;
            this.tbEntryPointUrl.Text = global::SES.Interop.API.UsersSwissKnife.Properties.Settings.Default.ApiUrl;
            // 
            // sfdResSave
            // 
            this.sfdResSave.DefaultExt = "txt";
            this.sfdResSave.Filter = "*.txt|*.txt";
            this.sfdResSave.RestoreDirectory = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(608, 440);
            this.Controls.Add(this.tcTabs);
            this.Controls.Add(this.tbUserName);
            this.Controls.Add(this.lUserName);
            this.Controls.Add(this.tbEntryPointUrl);
            this.Controls.Add(this.lEntryPoint);
            this.Name = "MainForm";
            this.Text = "Softengine SAP API :: User\'s Swiss Knife";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.tcTabs.ResumeLayout(false);
            this.tpKey.ResumeLayout(false);
            this.scKeys.Panel1.ResumeLayout(false);
            this.scKeys.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scKeys)).EndInit();
            this.scKeys.ResumeLayout(false);
            this.gbPrivKey.ResumeLayout(false);
            this.gbPrivKey.PerformLayout();
            this.gbPubKey.ResumeLayout(false);
            this.gbPubKey.PerformLayout();
            this.tpRequest.ResumeLayout(false);
            this.tpRequest.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudReqTimeout)).EndInit();
            this.tpResponse.ResumeLayout(false);
            this.tpResponse.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lEntryPoint;
        private System.Windows.Forms.TextBox tbEntryPointUrl;
        private System.Windows.Forms.Label lUserName;
        private System.Windows.Forms.TextBox tbUserName;
        private System.Windows.Forms.TabControl tcTabs;
        private System.Windows.Forms.TabPage tpKey;
        private System.Windows.Forms.TabPage tpRequest;
        private System.Windows.Forms.TabPage tpResponse;
        private System.Windows.Forms.Button bKeyRequestRenew;
        private System.Windows.Forms.Button bKeyOpen;
        private System.Windows.Forms.Button bKeySave;
        private System.Windows.Forms.Button bKeyNew;
        private System.Windows.Forms.GroupBox gbPubKey;
        private System.Windows.Forms.TextBox tbPubKey;
        private System.Windows.Forms.GroupBox gbPrivKey;
        private System.Windows.Forms.TextBox tbPrivKey;
        private System.Windows.Forms.SplitContainer scKeys;
        private System.Windows.Forms.Button bRespSave;
        private System.Windows.Forms.TextBox tbResponse;
        private System.Windows.Forms.Button bRespXml;
        private System.Windows.Forms.Button bRespJson;
        private System.Windows.Forms.TextBox tbReqSignature;
        private System.Windows.Forms.TextBox tbReqParams;
        private System.Windows.Forms.Label lReqSignature;
        private System.Windows.Forms.Label lReqParams;
        private System.Windows.Forms.Button bReqSend;
        private System.Windows.Forms.Button bReqLocalTimeRefresh;
        private System.Windows.Forms.Button bReqServerTimeRefresh;
        private System.Windows.Forms.TextBox tbReqLocalTime;
        private System.Windows.Forms.TextBox tbReqServerTime;
        private System.Windows.Forms.Label lReqLocalTime;
        private System.Windows.Forms.Label lReqServerTime;
        private System.Windows.Forms.Button bReqCommandsRefresh;
        private System.Windows.Forms.ComboBox cbReqCommand;
        private System.Windows.Forms.Label lReqCommand;
        private System.Windows.Forms.SaveFileDialog sfdKeySave;
        private System.Windows.Forms.OpenFileDialog ofdKeyOpen;
        private System.Windows.Forms.CheckBox cbReqUseActualTime;
        private System.Windows.Forms.Button bReqSignMessage;
        private System.Windows.Forms.CheckBox cbReqUseActualSignature;
        private System.Windows.Forms.TextBox tbResMessage;
        private System.Windows.Forms.Label lResMessage;
        private System.Windows.Forms.CheckBox cbResSuccess;
        private System.Windows.Forms.NumericUpDown nudReqTimeout;
        private System.Windows.Forms.Label lReqTimeout;
        private System.Windows.Forms.SaveFileDialog sfdResSave;
    }
}

